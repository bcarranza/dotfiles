# dotfiles

These are the dotfiles I use for my work here at GitLab. I also keep [helpful snippets](https://gitlab.com/bcarranza/dotfiles/-/snippets) in this project.