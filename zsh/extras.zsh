alias assh="autossh -M -0 $1"
alias girl="git"
alias gtlb="git clone git@gitlab.com:gitlab-org/gitlab.git"
alias hb="git clone git@gitlab.com:gitlab-com/content-sites/handbook.git"

## 📆 Notes and Tickets for Today

today () {
	takedir $(date +/Users/brie/%Y/%m-%B/%d-%A |  tr '[:upper:]' '[:lower:]')
}

## 🗓️ Weekly and Monthly Items

week () {
        takedir $(date +/Users/brie/%Y/%m-%B/week-%W |  tr '[:upper:]' '[:lower:]')
}

mnth () {
        takedir $(date +/Users/brie/%Y/%m-%B |  tr '[:upper:]' '[:lower:]')
}

## ✍️ Focus Sessions ✍️ ##
work() {
  # usage: work 10m, work 60s etc. Default is 20m
  timer "${1:-20m}" && terminal-notifier -message 'Pomodoro'\
        -title 'work == done: 🐈‍⬛ very good! --> take a break 😊'\
        -sound Crystal
}

rest() {
  # usage: rest 10m, rest 60s etc. Default is 5m
  timer "${1:-5m}" && terminal-notifier -message 'Pomodoro'\
        -title '💆 sorry but the break is over! get back to it pls 🐈‍⬛'\
        -sound Crystal
}

## ⏺️ DNS Records

geta () {
  while true ; do date ; echo ${1} ; echo "answer..." ; dig @1.1.1.1 ${1} -t a +short | lolcat ; sleep ${2:-2} ; done
}

digloop () {
  while true ; do date ; echo ${1} ; dig @1.1.1.1 ${1} -t ${2:-a} +short | lolcat ; sleep ${3:-5} ; done
}


HIST_STAMPS="yyyy-mm-dd"
# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"
